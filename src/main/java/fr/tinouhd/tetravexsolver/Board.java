package fr.tinouhd.tetravexsolver;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Board
{
	private final int size;

	private final List<Block> blocks = new ArrayList<>();
	private final Block[][] board;

	public Board(InputStream is) throws IOException
	{
		byte[] lengths = is.readNBytes(2);
		this.size = lengths[0] + lengths[1];

		this.board = new Block[size][size];

		for (int y = 0; y < size; y++)
	    {
			for (int x = 0; x < size; x++)
			{
				byte[] o = is.readNBytes(4);
				Block b = new Block(o[0], o[1], o[2], o[3]);

				blocks.add(b);
			}
		}
		Collections.sort(blocks);
	}

	public void solve()
	{
		solve(board, size, blocks);
	}

	private static boolean solve(Block[][] board, int size, List<Block> blocks)
	{
		for (int y = 0; y < size; y++)
		{
			for (int x = 0; x < size; x++)
			{
				if (board[y][x] == null)
				{
					int nbBlocks = blocks.size();
					for (int i = 0; i < nbBlocks; i++)
					{
						if (canPlace(board, blocks.get(i), x, y))
						{
							board[y][x] = blocks.remove(i);
							nbBlocks--;

							if (solve(board, size, blocks))
							{
								return true;
							} else
							{
								blocks.add(i, board[y][x]);
								nbBlocks++;
								board[y][x] = null;
							}
						}
					}
					return false;
				}
			}
		}

		return true;
	}

	private static boolean canPlace(Block[][] board, Block b, int x, int y)
	{
		return (x <= 0 || b.getSide(Side.LEFT) == board[y][x - 1].getSide(Side.RIGHT)) && (y <= 0 || b.getSide(Side.UP) == board[y - 1][x].getSide(Side.DOWN));
	}

	public Block getBlockAt(int x, int y)
	{
		return board[y][x];
	}

	public int getSize()
	{
		return size;
	}

	public byte[] toBinary()
	{
		byte[] bytes = new byte[size * size * 4];
		int i = 0;
		for (int y = 0; y < size; y++)
		{
			for (int x = 0; x < size; x++)
			{
				byte[] bBytes = getBlockAt(x, y).toBinary();
				for (byte bByte : bBytes)
				{
					bytes[i] = bByte;
					i++;
				}
			}
		}

		return bytes;
	}

	@Override public String toString()
	{
		return toString(board);
	}

	private String toString(Block[][] a)
	{
		StringBuilder out = new StringBuilder();
		for (Block[] value : a)
		{
			String line = "";
			for (Block block : value)
			{
				if (line.isEmpty())
				{
					line = block.toString();
				} else
				{
					line = putBlockOnRight(line, block);
				}
			}
			if (out.length() == 0)
			{
				out.append(line);
			} else
			{
				out.append("\n");
				out.append("=".repeat(line.split("\n")[0].length()));
				out.append("\n");
				out.append(line);
			}
		}
		return out.toString();
	}

	private String putBlockOnRight(String base, Block b)
	{
		String[] bases = base.split("\n");
		String[] bStrings = b.toString().split("\n");
		return bases[0] + "|" + bStrings[0] + "\n" + bases[1] + "|" + bStrings[1] + "\n" + bases[2] + "|" + bStrings[2] + "\n" + bases[3] + "|" + bStrings[3] + "\n" + bases[4] + "|" + bStrings[4];
	}

	private void printSolution(Block[][] a)
	{
		System.out.println(toString(a));
	}
}
