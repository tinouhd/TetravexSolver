package fr.tinouhd.tetravexsolver;

public enum Side
{
	LEFT((byte) 0),
	UP((byte) 1),
	RIGHT((byte) 2),
	DOWN((byte) 3);

	private final byte pos;

	Side(byte pos)
	{
		this.pos = pos;
	}

	public Side opposite()
	{
		switch (pos)
		{
			case 0:
				return RIGHT;
			case 1:
				return DOWN;
			case 2:
				return LEFT;
			default:
				return UP;
		}
	}

	public byte getBlockSide()
	{
		return pos;
	}
}
