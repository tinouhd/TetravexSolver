package fr.tinouhd.tetravexsolver;

public class Block implements Comparable<Block>
{
	private final byte[] numbers = new byte[4];

	public Block(byte left, byte up, byte right, byte down)
	{
		numbers[0] = left;
		numbers[1] = up;
		numbers[2] = right;
		numbers[3] = down;
	}

	public byte getSide(Side side)
	{
		return numbers[side.getBlockSide()];
	}

	private byte getUp()
	{
		return numbers[1];
	}

	private byte getLeft()
	{
		return numbers[0];
	}

	private byte getRight()
	{
		return numbers[2];
	}

	private byte getDown()
	{
		return numbers[3];
	}

	public byte[] toBinary()
	{
		return numbers;
	}

	@Override public String toString()
	{
		return "  " + getUp() + "  " + '\n' + " \\ / " + '\n' + getLeft() + "   " + getRight() + '\n' + " / \\ " + '\n' + "  " + getDown() + "  ";
	}

	@Override public boolean equals(Object o)
	{
		if (this == o)
			return true;
		if (o == null || getClass() != o.getClass())
			return false;
		Block block = (Block) o;

		if(numbers[0] == block.numbers[0] || numbers[0] == -1 || block.numbers[0] == -1)
		{
			if(numbers[1] == block.numbers[1] || numbers[1] == -1 || block.numbers[1] == -1)
			{
				if(numbers[2] == block.numbers[2] || numbers[2] == -1 || block.numbers[2] == -1)
				{
					if(numbers[3] == block.numbers[3] || numbers[3] == -1 || block.numbers[3] == -1)
					{
						return true;
					}
				}
			}
		}
		return false;
	}

	@Override public int compareTo(Block o)
	{
		if(this.equals(o)) return 0;
		else
		{
			int total = getDown() + getLeft() + getRight() + getUp();
			int total2 = o.getDown() + o.getLeft() + o.getRight() + o.getUp();
			return total - total2;
		}
	}
}
