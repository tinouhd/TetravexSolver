package fr.tinouhd.tetravexsolver;

public class Pair<E,F>
{
	private final E e;
	private final F f;

	public Pair(E e, F f)
	{
		this.e = e;
		this.f = f;
	}

	public E getE()
	{
		return e;
	}

	public F getF()
	{
		return f;
	}
}
