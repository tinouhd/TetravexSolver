package fr.tinouhd.tetravexsolver;

import java.io.IOException;

public class TetravexSolver
{
	public static void main(String[] args)
	{
		System.out.print('\0');

		try
		{
			Board b = new Board(/*TetravexSolver.class.getResourceAsStream("/8x8")*/ System.in);

//			System.out.println(b);
//			System.out.println(b.isSolved());
//			System.out.println("\n================================================================\n");
//			long start = System.nanoTime();
			b.solve();
//			long end = System.nanoTime();

//			Duration d = Duration.ofNanos(end - start);

//			System.out.println(d.toSeconds() + "s");
//			System.out.println(b);
//			System.out.println(b.isSolved());
			System.out.print(new String(b.toBinary()));

		} catch (IOException e)
		{
			e.printStackTrace();
		}
	}
}
